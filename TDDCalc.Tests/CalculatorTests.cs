﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TDDCalc.Tests
{
    public class CalculatorTests
    {
        [Fact]
        public void EmptyStringShouldReturnZero()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("");

            Assert.Equal(0, result);
        }

        [Fact]
        public void SingleNumberShouldBeParsedToInteeger()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("420");

            Assert.Equal(420, result);
        }

        [Fact]
        public void TwoNumbersCommaDelimitedShouldReturnTheirSum()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("666,3");

            Assert.Equal(669, result);
        }

        [Fact]
        public void TwoNumbersNewLineDelimitedShouldReturnTheirSum()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("666\n3");

            Assert.Equal(669, result);
        }

        [Fact]
        public void ThreeNumbersNewLineDelimitedShouldReturnTheirSum()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("999\n999\n999");

            Assert.Equal(2997, result);
        }

        [Fact]
        public void ThreeNumbersCommaDelimitedShouldReturnTheirSum()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("100,10,1");

            Assert.Equal(111, result);
        }


        [Fact]
        public void SpecifingDelimiterCharShouldUseItInsteadOfDefault()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("//#\n100#10#1");

            Assert.Equal(111, result);
        }

        [Fact]
        public void UsingCommaDelimiterWhenSpecifingDelimiterCharShouldFail()
        {
            ICalculator calculator = new Calculator();

            Action act = () => { calculator.Calculate("//#\n100,10,1"); };

            Assert.Throws<FormatException>(act);
        }

        [Fact]
        public void SpecifingDelimiterStringShouldUseItInsteadOfDefault()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("//[###]\n600###60###6");

            Assert.Equal(666, result);
        }

        [Fact]
        public void NegativeNumberInInputShouldFail()
        {
            ICalculator calculator = new Calculator();

            Action act = () => { calculator.Calculate("4,-2,1"); };

            Assert.Throws<FormatException>(act);
        }


        [Fact]
        public void LargeNumbersInInputShouldBeignored()
        {
            ICalculator calculator = new Calculator();

            int result = calculator.Calculate("1000,1001,997");

            Assert.Equal(1997, result);
        }
    }
}
