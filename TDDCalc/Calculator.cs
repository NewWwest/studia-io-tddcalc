﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace TDDCalc
{
    public class Calculator : ICalculator
    {
        public int Calculate(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            if (string.IsNullOrEmpty(input))
                return 0;

            string[] delimiters = null;

            if (input.StartsWith("//"))
            {
                string customDel = input.Substring(2, input.IndexOf('\n') - 2);

                if (customDel.Length == 0)
                    throw new FormatException();

                if (customDel.Length == 1)
                {
                    delimiters = new string[] { customDel };
                }
                else
                {
                    if (!customDel.StartsWith('[') || !customDel.EndsWith(']'))
                        throw new FormatException();

                    delimiters = new string[] { customDel.Substring(1, customDel.Length - 2) };
                }

                input = input.Substring(input.IndexOf('\n') + 1);
            }
            else
            {
                delimiters = new string[] { ",", "\n" };
            }

            string[] numbers = input.Split(delimiters, new StringSplitOptions());
            if (numbers.Length > 3)
                throw new FormatException();

            int result = 0;
            foreach (var number in numbers)
            {
                if (number.Any((c) => !char.IsDigit(c)))
                    throw new FormatException();
                
                if (number.Length > 5)
                    continue;

                int parsed = int.Parse(number);
                if (parsed > 1000)
                    continue;

                result += parsed;
            }
            return result;
        }
    }
}
