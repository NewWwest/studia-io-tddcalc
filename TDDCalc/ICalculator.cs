﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDDCalc
{
    public interface ICalculator
    {
        int Calculate(string input);
    }
}
